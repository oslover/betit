//
//  AppManager.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit
import ObjectMapper

class AppManager: NSObject {
    static let shared = AppManager()
    
    var isGuest: Bool = false
    
    var isLoggedIn: Bool = false
    
    override init() {
        super.init()
        
        loadUser()
    }
    
    // Current User
    var currentUser : User?
    var loggedIn: Bool{
        guard let user = currentUser else {
            return false
        }
        
        guard user.id != nil, let token = user.token, token.accessToken != nil else {
            return false
        }
        return true
    }

    func loadUser() {
        if let json = KeychainManager.getString(for: "current_user"), json.count > 0 {
            let mapper = Mapper<User>()
            currentUser = mapper.map(JSONString: json)
        }
        else {
            currentUser = User()
        }
    }
    
    func saveUser(){
        KeychainManager.setString(value: currentUser?.toJSONString() ?? "", for: "current_user")
    }
    
    func saveCurrentUser(user: User) {
        if let currentUser = self.currentUser {
            currentUser.attach(user)
        }
        else {
            self.currentUser = user
        }
        saveUser()
    }
    
    //Bets
    var bets: [Bet] = Bet.samples() ?? []
}

extension AppManager {
    func showNext(animated: Bool = false) {
        if isLoggedIn {
            UIManager.showMain(animated: animated)
        }
        else {
            UIManager.showLogin(animated: animated)
        }
    }
}

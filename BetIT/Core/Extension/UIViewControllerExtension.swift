//
//  UIViewControllerExtension.swift
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

extension UIViewController {
    func isRootController() -> Bool {
        let vc = self.navigationController?.viewControllers.first
        if self == vc {
            return true
        }
        return false
    }
}

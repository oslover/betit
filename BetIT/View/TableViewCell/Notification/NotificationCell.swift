//
//  BetCell.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/2/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class NotificationCell: BaseTableViewCell {
    override class var identifier: String {
        return "notification_cell"
    }
    
    override class var height: CGFloat {
        return 80
    }
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    func reset(with notification: BetNotification) {
        if let user = notification.bet?.opponent {
            imgUser.loadImage(url: user.thumbnail)
        }
        lblContent.attributedText = notification.attributedContentForNotificationCell()
        lblTime.text = notification.time.periodStringSince()
    }
}

//Attributed string for NotificationCell
extension BetNotification {
    func attributedContentForNotificationCell() -> NSAttributedString {
        guard let bet = self.bet else {
            return NSAttributedString(string: "")
        }
        
        let userName = bet.opponent.fullName
        let betTitle = bet.title
        
        let fullText = String(format: type.format, userName ?? "", betTitle ?? "")
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineHeightMultiple = 1.4
        let attributeForAll = [NSAttributedString.Key.paragraphStyle: paragraphStyle,
                               NSAttributedString.Key.foregroundColor: UIColor.black,
                               NSAttributedString.Key.font: AppFont.interstate.light(size: 12)]
        
        let attributedString = NSMutableAttributedString(string: fullText, attributes: attributeForAll)
        if let rangeUserName = fullText.range(of: userName ?? "INVALID_STRING") {
            attributedString.addAttributes([NSAttributedString.Key.font: AppFont.interstate.regular(size: 12)], range: NSRange(rangeUserName, in: fullText))
        }
        
        if let rangeBetTitle = fullText.range(of: betTitle ?? "INVALID_STRING") {
            attributedString.addAttributes([NSAttributedString.Key.font: AppFont.interstate.regular(size: 12)], range: NSRange(rangeBetTitle, in: fullText))
        }
        return attributedString
    }
}

//
//  BetCell.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/2/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class BetCell: BaseTableViewCell {
    override class var identifier: String {
        return "bet_cell"
    }
    
    override class var height: CGFloat {
        return 135
    }
    
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblBetTitle: UILabel!
    @IBOutlet weak var lblBetDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    func reset(with bet: Bet) {
        imgUser?.loadImage(url: bet.opponent.thumbnail)
        lblUserName?.text = bet.opponent.fullName
        
        guard let status = bet.status else {
            return
        }
        imgStatus?.image = status.gradientImage
        lblBetTitle?.text = bet.title
        
        switch status {
        case .live, .pendingBetActionNeeded, .expired:
            lblStatus?.text = bet.status.title
            lblStatus?.textColor = bet.status.color
        case .opponentClaimedWin, .youClaimedWin:
            lblBetDescription?.text = (bet.opponent.fullName ?? "") + " " + "claimed_win".localized()
        case .disputed:
            lblBetDescription?.text = (bet.opponent.fullName ?? "") + " " + "denied_your_claim".localized()
        case .endActionConfirmNeeded:
            lblBetDescription?.text = (bet.opponent.fullName ?? "") + " " + "Settle is needed"
        default:
            break
        }
    }
}

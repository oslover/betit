//
//  BetSectionHeader.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/2/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class BetSectionHeader: BaseHeaderFooterView {
    override class var identifier: String {
        return "base_header_footer"
    }
    
    var lblTitle: UILabel!

    override func configure() {
        super.configure()
        
        if lblTitle == nil {
            lblTitle = UILabel(font: AppFont.interstate.light(size: 12), color: .lightGray)
            lblTitle.backgroundColor = .clear
            self.addSubview(lblTitle)
            lblTitle.snp.makeConstraints {
                $0.left.equalToSuperview().offset(12)
                $0.centerY.equalToSuperview()
            }
        }
    }
    
    func reset(with title: String) {
        lblTitle?.text = title
    }
}

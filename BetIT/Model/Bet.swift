//
//  Bet.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/2/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit
import ObjectMapper

class Bet: BaseObject {
    var title: String!
    var description: String!
    var wager: String!
    var status: BetStatus!
    var deadline: Date!
    var opponent: User!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        title                       <- map["title"]
        wager                       <- map["wager"]
        description                 <- map["description"]
        status                      <- (map["status"], EnumTransform<BetStatus>())
        deadline                    <- (map["deadline"], DateTransform.shared)
        opponent                    <- map["opponent"]
    }
}

enum BetCategory: Int {
    case live = 0
    case unsettled = 1
    case settled = 2
    
    var title: String {
        switch self {
        case .live:
            return "live".localized()
        case .unsettled:
            return "unsettled".localized()
        case .settled:
            return "settled".localized()
        }
    }
    
    static var all: [BetCategory] {
        return [.live, .unsettled, .settled]
    }
}

enum BetStatus: Int {
    case live = 0
    case pendingBetActionNeeded = 1
    case betWon = 2
    case opponentClaimedWin = 3
    case expired = 4
    case youClaimedWin = 5
    case betLost = 6
    case disputed = 7
    case declined = 8
    case endActionConfirmNeeded = 9
    
    var title: String? {
        switch self {
        case .live:
            return "live".localized()
        case .pendingBetActionNeeded:
            return "pending_bet_action_needed".localized()
        case .betWon:
            return "bet_won".localized()
        case .betLost:
            return "bet_lost".localized()
        case .opponentClaimedWin:
            return "claimed_win".localized()
        case .expired:
            return "expired".localized()
        case .youClaimedWin:
            return "claimed_win".localized()
        case .disputed:
            return "disputed".localized()
        case .declined:
            return "declined".localized()
        case .endActionConfirmNeeded:
            return "declined".localized()
        }
    }
    
    var color: UIColor {
        switch self {
        case .live:
            return #colorLiteral(red: 0.1215686275, green: 0.6156862745, blue: 0, alpha: 1)
        case .pendingBetActionNeeded:
            return #colorLiteral(red: 0.968627451, green: 0.7098039216, blue: 0, alpha: 1)
        case .betWon:
            return #colorLiteral(red: 0.6, green: 0.8274509804, blue: 0.9254901961, alpha: 1)
        case .opponentClaimedWin:
            return #colorLiteral(red: 0.4862745098, green: 0.2392156863, blue: 0.7882352941, alpha: 1)
        case .expired:
            return #colorLiteral(red: 0.462745098, green: 0.4588235294, blue: 0.462745098, alpha: 1)
        case .youClaimedWin:
            return #colorLiteral(red: 0.5960784314, green: 0.6549019608, blue: 0.08235294118, alpha: 1)
        case .betLost:
            return #colorLiteral(red: 0.06274509804, green: 0.1215686275, blue: 0.2901960784, alpha: 1)
        case .disputed:
            return #colorLiteral(red: 0.8784313725, green: 0.1254901961, blue: 0.1254901961, alpha: 1)
        case .declined:
            return #colorLiteral(red: 0.462745098, green: 0.4588235294, blue: 0.462745098, alpha: 1)
        case .endActionConfirmNeeded:
            return .clear
        }
    }
    
    var statusImage: UIImage? {
        switch self {
        case .live:
            return UIImage(named: "img_gradient_green_horz")
        case .pendingBetActionNeeded:
            return UIImage(named: "img_gradient_yellow_horz")
        case .betWon:
            return UIImage(named: "img_gradient_cyan_horz")
        case .opponentClaimedWin:
            return nil
        case .expired:
            return nil
        case .youClaimedWin:
            return UIImage(named: "img_gradient_cyan_horz")
        case .betLost:
            return nil
        case .disputed:
            return UIImage(named: "img_gradient_red_horz")
        case .declined:
            return UIImage(named: "img_gradient_vert_gray")
        default:
            return nil
        }
    }
    
    var statusColor: UIColor? {
        switch self {
        case .live:
            return .darkForestGreen
        case .pendingBetActionNeeded:
            return .yellow
        case .betWon:
            return .lightCyan
        case .opponentClaimedWin:
            return .darkBlue
        case .expired:
            return nil
        case .youClaimedWin:
            return .cyan
        case .betLost:
            return .darkBlue
        case .disputed:
            return .lightRed
        case .declined:
            return .black
        default:
            return nil
        }
    }
    
    var category: BetCategory {
        switch self {
        case .live, .pendingBetActionNeeded, .expired:
            return .live
        case .betWon, .betLost:
            return .settled
        default:
            return .unsettled
        }
    }
    
    var section: String {
        switch self {
        case .live:
            return "active_bets".localized()
        case .pendingBetActionNeeded:
            return "bet_requests".localized()
        case .betWon:
            return "bets_won".localized()
        case .betLost:
            return "bets_lost".localized()
        case .opponentClaimedWin, .youClaimedWin:
            return "claims".localized()
        case .disputed:
            return "disputed".localized()
        case .expired, .declined, .endActionConfirmNeeded:
            return "wagers".localized()
        }
    }
    
    var gradientImage: UIImage? {
        switch self {
        case .live:
            return UIImage(named: "img_gradient_vert_green")
        case .pendingBetActionNeeded:
            return UIImage(named: "img_gradient_vert_yellow")
        case .betWon:
            return UIImage(named: "img_gradient_vert_cyan")
        case .opponentClaimedWin:
            return UIImage(named: "img_gradient_vert_purple")
        case .expired:
            return UIImage(named: "img_gradient_vert_gray")
        case .youClaimedWin:
            return UIImage(named: "img_gradient_vert_darkyellow")
        case .betLost:
            return UIImage(named: "img_gradient_vert_black")
        case .disputed:
            return UIImage(named: "img_gradient_vert_red")
        case .declined:
            return UIImage(named: "img_gradient_vert_gray")
        default:
            return nil
        }
    }
}

extension Bet {
    static func samples() -> [Bet]? {
        let mapper = Mapper<Bet>()
        return mapper.mapArray(JSONString: jsonBets)
    }
}

let jsonBets = """
[

{
    "id": 1,
    "title": "Soccer - Barcelona vs. liverpool",
    "description": "I bet the Eagles will win by 10 pts",
    "deadline": 1565336140,
    "wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
    "status": 0,
    "opponent": {
    "id": 1,
    "first_name": "Noah",
    "last_name": "Rios",
    "thumbnail": "https://res.cloudinary.com/demo/image/upload/c_thumb,g_face,w_200,h_200/lady.jpg"
    }
},
{
    "id": 2,
    "title": "Soccer - Barcelona vs. liverpool",
    "description": "I bet the Eagles will win by 10 pts",
    "deadline": 1565336140,
    "wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
    "status": 0,
    "opponent": {
    "id": 2,
    "first_name": "Andrew",
    "last_name": "Hale",
    "thumbnail": "https://hips.hearstapps.com/rover/profile_photos/d494ffe7-cb1f-40ea-b0fb-9d02e8616db6.jpg"
    }
},


{
    "id": 3,
    "title": "Raptors vs. Warriors game 3",
    "description": "I bet the Eagles will win by 10 pts",
    "deadline": 1565764960,
    "wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
    "status": 1,
    "opponent": {
    "id": 3,
    "first_name": "Michael",
    "last_name": "Jordan",
    "thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
    }
},
{
    "id": 4,
    "title": "Eagles vs. Patriots game",
    "description": "I bet the Eagles will win by 10 pts",
    "deadline": 1565499160,
    "wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
    "status": 1,
    "opponent": {
    "id": 4,
    "first_name": "Ryan",
    "last_name": "Pearson",
    "thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
    }
},
{
"id": 5,
"title": "Mini-golf",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565499160,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 1,
"opponent": {
"id": 5,
"first_name": "Clara",
"last_name": "Griffin",
"thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
}
},


{
"id": 6,
"title": "Raptors vs. Warriors game 3",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 3,
"opponent": {
"id": 3,
"first_name": "Michael",
"last_name": "Jordan",
"thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
}
},
{
"id": 7,
"title": "Eagles vs. Patriots game",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 5,
"opponent": {
"id": 3,
"first_name": "Michael",
"last_name": "Jordan",
"thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
}
},


{
"id": 8,
"title": "Soccer - Barcelona vs. liverpool",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565336140,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 7,
"opponent": {
"id": 2,
"first_name": "Andrew",
"last_name": "Hale",
"thumbnail": "https://hips.hearstapps.com/rover/profile_photos/d494ffe7-cb1f-40ea-b0fb-9d02e8616db6.jpg"
}
},


{
"id": 9,
"title": "Raptors vs. Warriors game 3",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 9,
"opponent": {
"id": 3,
"first_name": "Michael",
"last_name": "Jordan",
"thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
}
},
{
"id": 10,
"title": "Mini-golf",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 9,
"opponent": {
"id": 5,
"first_name": "Clara",
"last_name": "Griffin",
"thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
}
},


{
"id": 11,
"title": "Raptors vs. Warriors game 3",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 2,
"opponent": {
"id": 3,
"first_name": "Michael",
"last_name": "Jordan",
"thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
}
},
{
"id": 12,
"title": "Mini-golf",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 2,
"opponent": {
"id": 5,
"first_name": "Clara",
"last_name": "Griffin",
"thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
}
},
{
"id": 13,
"title": "Raptors vs. Warriors game 3",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 2,
"opponent": {
"id": 3,
"first_name": "Michael",
"last_name": "Jordan",
"thumbnail": "https://static1.fjcdn.com/comments/Get+called+sexy+by+_849167e5eeca887317f7c450124c0467.jpg"
}
},
{
"id": 14,
"title": "Mini-golf",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 6,
"opponent": {
"id": 5,
"first_name": "Clara",
"last_name": "Griffin",
"thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
}
},
{
"id": 15,
"title": "Mini-golf",
"description": "I bet the Eagles will win by 10 pts",
"deadline": 1565764960,
"wager": "If I win Joe will have to go to work dressed as a disney princess. If he wins, I will shave my head",
"status": 6,
"opponent": {
"id": 5,
"first_name": "Clara",
"last_name": "Griffin",
"thumbnail": "https://mffk.org/wp-content/uploads/2018/08/person-2-200x200.jpg"
}
}
]
"""

//
//  Bet.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/6/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit
import ObjectMapper

enum BetNotificationType: Int {
    case accepted = 0
    case challengeRequested = 1
    case opponentClaimedLost = 2
    case opponentClaimedWin = 3
    case expired = 4
    case disputed = 5
    case declined = 6
    
    var format: String {
        switch self {
        case .accepted:
            return "notification_accepted".localized()
        case .challengeRequested:
            return "notification_challengeRequested".localized()
        case .opponentClaimedLost:
            return "notification_opponentClaimedLost".localized()
        case .opponentClaimedWin:
            return "notification_opponentClaimedWin".localized()
        case .expired:
            return "notification_expired".localized()
        case .disputed:
            return "notification_disputed".localized()
        case .declined:
            return "notification_declined".localized()
        }
    }
}

class BetNotification: BaseObject {
    var betId: Int!
    var type: BetNotificationType!
    var time: Date!
    var bet: Bet?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        betId                       <- map["bet_id"]
        type                        <- (map["type"], EnumTransform<BetNotificationType>())
        time                        <- (map["time"], DateTransform.shared)
    }
}

extension BetNotification {
    static func samples() -> [BetNotification]? {
        let mapper = Mapper<BetNotification>()
        return mapper.mapArray(JSONString: jsonNotifications)
    }
}

let jsonNotifications = """
[
{
"id": 1,
"bet_id": 1,
"time": 1565336140,
"type": 0
},
{
"id": 5,
"bet_id": 5,
"time": 1565336140,
"type": 5
},
{
"id": 2,
"bet_id": 2,
"time": 1565764960,
"type": 1
},
{
"id": 3,
"bet_id": 3,
"time": 1565499160,
"type": 2
},
{
"id": 4,
"bet_id": 4,
"time": 1565613760,
"type": 3
},
{
"id": 6,
"bet_id": 6,
"time": 1565408560,
"type": 4
}
]
"""

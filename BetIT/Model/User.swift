//
//  User.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//
import UIKit
import ObjectMapper
import SwiftyJSON

class User: BaseObject {
    // User property
    var firstName: String?
    var lastName: String?
    var thumbnail: URL?
    var token: Token?
    
    var fullName: String? {
        return (firstName ?? "") + " " + (lastName ?? "")
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)

        firstName                   <- map["first_name"]
        lastName                    <- map["last_name"]
        thumbnail                   <- (map["thumbnail"], URLTransform.shared)
    }
    
    override func attach(_ model: BaseObject) {
        super.attach(model)
        guard let user = model as? User else {
            return
        }
        
        firstName         = user.firstName ?? firstName
        lastName         = user.lastName ?? lastName
        thumbnail         = user.thumbnail ?? thumbnail
    }
}

extension User {
    func isValidProfile() -> Bool {
        guard firstName != nil else {
            return false
        }
        return true
    }
}


//
//  SelectDateTimeViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/2/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit
import FSCalendar

class SelectDateTimeViewController: ModalViewController {
    @IBOutlet weak var calendar: FSCalendar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.appearance.headerMinimumDissolvedAlpha = 0;
        calendar.appearance.caseOptions = .weekdayUsesSingleUpperCase;
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onPrev(_ sender: Any) {
        let date = calendar.currentPage
        calendar.currentPage = date.adding(.month, value: -1)
    }
    
    @IBAction func onNext(_ sender: Any) {
        let date = calendar.currentPage
        calendar.currentPage = date.adding(.month, value: 1)
    }
}


//
//  BetDetailViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class BetDetailViewController: BaseViewController {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var txtTitleOfBet: UITextField!
    @IBOutlet weak var txtBetDescription: UITextField!
    @IBOutlet weak var lblBetWager: UILabel!
    @IBOutlet weak var txtBetWager: UITextView!
    @IBOutlet weak var btnBetDeadline: UIButton!
    @IBOutlet weak var lblBetTimeRemaining: UILabel!
    @IBOutlet weak var lblBetStatus: UILabel!
    
    @IBOutlet weak var btnEditBet: UIButton!
    @IBOutlet weak var stackSaveEdit: UIStackView!
    
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var notificationHeight: NSLayoutConstraint!
    
    override var isEditing: Bool {
        didSet {
            btnEditBet.isHidden = isEditing
            stackSaveEdit.isHidden = !isEditing
            btnBetDeadline.isEnabled = isEditing
        }
    }
    
    var bet: Bet?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadBet()
    }
    
    func loadBet() {
        notificationHeight.constant = 0
        
        guard let bet = self.bet else {
            return
        }
        
        self.title = bet.title
        
        imgUser.loadImage(url: bet.opponent.thumbnail)
        lblUsername.text = bet.opponent.fullName
        txtTitleOfBet.text = bet.title
        txtBetDescription.text = bet.description
        lblBetWager.text = bet.wager
        lblBetWager.setLineSpacing(lineHeightMultiple: 2.0)
        txtBetWager.text = bet.wager
        
        btnBetDeadline.setTitle(bet.deadline.string(withFormat: Constant.Format.betTime), for: .normal)
        let daysRemaining = Int(bet.deadline.daysSince(Date()))
        lblBetTimeRemaining.text = "\(daysRemaining) days\nremaining"
    }
    
    @IBAction func onDeadlineChange(_ sender: Any) {
        guard let controller = UIManager.loadViewController(storyboard: "Bet", controller: "sid_select_datetime") as? SelectDateTimeViewController else {
            return
        }
        
        UIManager.modal(controller: controller, parentController: self)
    }
    
    @IBAction func onDelete(_ sender: Any) {
        UIManager.showAlert(title: "Delete Bet", message: "Are you sure you want to \n delete this bet?", buttons: ["Cancel", "Delete"], completion: { (index) in
            //alert ended with index
        }, parentController: self)
    }
    
    @IBAction func onEditBet(_ sender: Any) {
        isEditing = true
    }
    
    @IBAction func onSaveEdit(_ sender: Any) {
        isEditing = false
    }
    
    @IBAction func onCancelEdit(_ sender: Any) {
        isEditing = false
    }
}

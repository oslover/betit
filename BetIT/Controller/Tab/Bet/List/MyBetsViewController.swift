//
//  MyBetsViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class MyBetsViewController: BaseViewController {
    @IBOutlet weak var segment: BarSegmentControl!
    @IBOutlet weak var viewStartBet: UIView!
    @IBOutlet weak var tblBets: UITableView!
    
    var bets: [String: [Bet]] = [:]
    var sortedSection: [String] = []
    
    var currentCategory: BetCategory = .live {
        didSet {
            loadBets()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.currentCategory = .live
        // Do any additional setup after loading the view.
    }
    
    override func configureUI() {
        super.configureUI()
        
        segment.items = [
            (title: BetCategory.live.title.uppercased(), alignment: .left),
            (title: BetCategory.unsettled.title.uppercased(), alignment: .center),
            (title: BetCategory.settled.title.uppercased(), alignment: .right)
        ]
        segment.index = BetCategory.live.rawValue
        
        segment.selector = { [weak self] index in
            guard let weak_self = self else {
                return
            }
            weak_self.currentCategory = BetCategory(rawValue: index) ?? .live
        }
        
        BetCell.registerWithNib(to: tblBets)
        BetCell.registerWithNib(to: tblBets, nibName: "BetCellConfirm", identifier: "bet_cell_confirm")
        BetCell.registerWithNib(to: tblBets, nibName: "BetCellSettle", identifier: "bet_cell_settle")
        BetCell.registerWithNib(to: tblBets, nibName: "BetCellSimple", identifier: "bet_cell_simple")
        BetSectionHeader.register(to: tblBets)
    }
    
    func loadBets() {
        self.bets.removeAll()
        self.sortedSection.removeAll()
        
        let bets = AppManager.shared.bets.filter {
            $0.status.category == currentCategory
        }
        
        bets.forEach { (bet) in
            if let status = bet.status {
                var existing = self.bets[status.section] ?? []
                existing.append(bet)
                self.bets[status.section] = existing
            }
        }
        sortedSection = self.bets.keys.sorted()
        viewStartBet.isHidden = bets.count > 0
        tblBets.reloadData()
    }
    
    @IBAction func onNotifications(_ sender: Any) {
        guard let controller = UIManager.loadViewController(storyboard: "Notifications", controller: "sid_notifications") else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sid_bet_detail" {
            if let controller = segue.destination as? BetDetailViewController, let indexPath = sender as? IndexPath {
                let section = self.sortedSection[indexPath.section]
                controller.bet = bets[section]?[indexPath.row]
            }
        }
    }
}

extension BetStatus {
    var cellIdentifier: String {
        switch self {
        case .live, .pendingBetActionNeeded, .expired:
            return "bet_cell"
        case .opponentClaimedWin, .youClaimedWin:
            return "bet_cell_confirm"
        case .disputed, .betLost, .betWon:
            return "bet_cell_simple"
        default:
            return "bet_cell_settle"
        }
    }
}

extension MyBetsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bets[self.sortedSection[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let bet = bets[self.sortedSection[indexPath.section]]?[indexPath.row], let status = bet.status else {
            return UITableViewCell()
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: status.cellIdentifier) as! BetCell
        cell.reset(with: bet)
        return cell
    }
    
    //Section & HeaderView Configuration
    func numberOfSections(in tableView: UITableView) -> Int {
        return sortedSection.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let count = self.tableView(tableView, numberOfRowsInSection: section)
        
        return (count > 0) ? 30 : 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            header.backgroundView?.backgroundColor = UIColor.clear
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: BetSectionHeader.identifier) as? BetSectionHeader
        headerView?.reset(with: sortedSection[section])
        return headerView
    }
}

extension MyBetsViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return BetCell.height
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sid_bet_detail", sender: indexPath)
    }
}

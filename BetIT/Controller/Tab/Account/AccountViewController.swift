//
//  AccountViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

typealias AccountItem = (title: String, segue: String?)

class AccountViewController: BaseViewController {
    @IBOutlet weak var tblAccount: UITableView!
    var accountItems: [AccountItem] = []

    override func configureUI() {
        super.configureUI()
        
        AccountCell.registerWithNib(to: tblAccount)
        accountItems = [
            (title: "edit_profile".localized(), segue: "sid_edit_profile"),
            (title: "notification_settings".localized(), segue: "sid_notification_settings"),
            (title: "invite_friends".localized(), segue: "sid_invite_friends"),
            (title: "support_faq".localized(), segue: nil),
            (title: "terms_privacy".localized(), segue: nil),
            (title: "logout".localized(), segue: nil)
        ]
        tblAccount.reloadData()
    }
}


extension AccountViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AccountCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountCell.identifier) as! AccountCell
        cell.reset(with: accountItems[indexPath.row].title)
        return cell
    }
}

extension AccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = accountItems[indexPath.row]
        if let sid = item.segue {
            self.performSegue(withIdentifier: sid, sender: self)
        }
        else {
            if item.title == "logout".localized() {
                AppManager.shared.isLoggedIn = false
                AppManager.shared.showNext(animated: true)
            }
        }
    }
}


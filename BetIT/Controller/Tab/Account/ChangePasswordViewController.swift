//
//  ChangePasswordViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {
    @IBOutlet weak var viewContainer: UIView!
    var isAnimated: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isAnimated == false {
            self.viewContainer.frame = CGRect(x: 0, y: self.view.bounds.height, width: viewContainer.bounds.width, height: viewContainer.bounds.height)
            UIView.animate(withDuration: 0.3) {
                self.viewContainer.frame = CGRect(x: 0, y: 54, width: self.viewContainer.bounds.width, height: self.viewContainer.bounds.height)
            }
        }
    }
}

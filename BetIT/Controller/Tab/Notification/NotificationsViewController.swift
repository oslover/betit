//
//  NotificationsViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/6/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {
    @IBOutlet weak var tblNotification: UITableView!
    var notifications: [BetNotification] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadNotifications()
        // Do any additional setup after loading the view.
    }
    
    override func configureUI() {
        super.configureUI()
        
        NotificationCell.registerWithNib(to: tblNotification)
    }
    
    func loadNotifications() {
        notifications = BetNotification.samples() ?? []
        notifications.forEach { notification in
            notification.bet = AppManager.shared.bets.filter {
                $0.id == notification.betId
            }.first
        }
        tblNotification.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sid_bet_state", let indexPath = sender as? IndexPath {
            let controller = segue.destination as! BetStateViewController
            controller.bet = notifications[indexPath.row].bet
        }
    }
}

extension NotificationsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier) as! NotificationCell
        cell.reset(with: notifications[indexPath.row])
        cell.contentView.backgroundColor = (indexPath.row%2 == 0) ? #colorLiteral(red: 0.9294117647, green: 0.9607843137, blue: 0.9843137255, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
}

extension NotificationsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return NotificationCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sid_bet_state", sender: indexPath)
    }
}

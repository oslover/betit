//
//  BetStateViewController.swift
//  BetIT
//
//  Created by Cesar Villamil on 8/9/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//

import UIKit

class BetStateViewController: BaseViewController {
    var bet: Bet?
    
    //Bet property
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblBetDescription: UILabel!
    @IBOutlet weak var lblWager: UILabel!
    
    @IBOutlet weak var viewDeadline: UIStackView!
    @IBOutlet weak var btnBetDeadline: UIButton!
    @IBOutlet weak var lblBetTimeRemaining: UILabel!
    
    //Containers
    @IBOutlet weak var viewDoOrCancel: UIStackView!
    @IBOutlet weak var btnDo: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var viewClaim: UIView!
    @IBOutlet weak var lblClaim: UILabel!
    @IBOutlet weak var lblClaimDescription: UILabel!
    
    @IBOutlet weak var viewCongratulations: UIStackView!
    
    @IBOutlet weak var viewAlert: UIStackView!
    @IBOutlet weak var lblAlertDescription: UILabel!
    
    @IBOutlet weak var viewBlackGreenButtons: UIStackView!
    @IBOutlet weak var btnBlack: UIButton!
    @IBOutlet weak var btnGreen: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func configureUI() {
        super.configureUI()
        
        loadBetInfo()
        loadStatus()
        loadActions()
    }
    
    func loadBetInfo() {
        guard let bet = self.bet else {
            return
        }
        self.title = bet.title
        lblUserName.text = bet.opponent?.fullName
        imgUser.loadImage(url: bet.opponent?.thumbnail)
        lblBetDescription.text = bet.description
        lblWager.text = bet.wager
        
        btnBetDeadline.setTitle(bet.deadline.string(withFormat: Constant.Format.betTime), for: .normal)
        let daysRemaining = Int(bet.deadline.daysSince(Date()))
        lblBetTimeRemaining.text = "\(daysRemaining) days\nremaining"
    }
    
    func loadStatus() {
        guard let bet = self.bet else {
            return
        }
        
        viewStatus.isHidden = true
        if let image = bet.status?.statusImage {
            viewStatus.isHidden = false
            imgStatus.image = image
        }
        
        if let color = bet.status?.statusColor {
            viewStatus.isHidden = false
            imgStatus.backgroundColor = color
        }
        
        if let status = bet.status?.title {
            lblStatus.text = "Status: " + status
        }
    }
    
    func loadActions() {
        guard let bet = self.bet, let status = bet.status else {
            return
        }
        
        viewDeadline.isHidden = true
        viewDoOrCancel.isHidden = true
        viewClaim.isHidden = true
        viewCongratulations.isHidden = true
        viewAlert.isHidden = true
        viewBlackGreenButtons.isHidden = true
        
        switch status {
        case .live:
            viewAlert.isHidden = false
            lblAlertDescription.text = "bet_accepted_description".localized()
            viewBlackGreenButtons.isHidden = false
            btnBlack.setTitle("i_lost".localized(), for: .normal)
            btnGreen.setTitle("i_won".localized(), for: .normal)
        case .pendingBetActionNeeded:
            viewDeadline.isHidden = false
            viewDoOrCancel.isHidden = false
            btnDo.setTitle("accept_bet".localized(), for: .normal)
            btnCancel.setTitle("decline_bet".localized(), for: .normal)
        case .betWon:
            viewCongratulations.isHidden = false
        case .opponentClaimedWin:
            viewDeadline.isHidden = false
            viewDoOrCancel.isHidden = false
            btnDo.setTitle("confirm_their_win".localized(), for: .normal)
            btnCancel.setTitle("dispute".localized(), for: .normal)
        case .expired:
            break
        case .youClaimedWin:
            viewClaim.isHidden = false
            lblClaim.text = "winning_claimed".localized()
            lblClaimDescription.text = "winning_claimed_description".localized()
        case .betLost:
            break
        case .disputed:
            viewClaim.isHidden = false
            lblClaim.text = "bet_disputed".localized()
            lblClaimDescription.text = "claim_again".localized()
            
            viewBlackGreenButtons.isHidden = false
            btnBlack.setTitle("i_lost".localized(), for: .normal)
            btnGreen.setTitle("i_won".localized(), for: .normal)
        case .declined:
            break
        default:
            break
        }
    }
    
    //View Black Green Buttons
    @IBAction func onBlack(_ sender: Any) {
        guard let bet = self.bet, let status = bet.status else {
            return
        }
        
        switch status {
        case .live:
            UIManager.showAlert(title: "womp_womp".localized(), message: "loser_confirm_description".localized(), buttons: ["cancel".localized(), "yes".localized()], completion: { (index) in
                if index == 1 {
                    self.bet?.status = .betLost
                    self.loadStatus()
                }
                self.viewClaim.isHidden = false
                self.lblClaim.text = "lost_claimed".localized()
                self.lblClaimDescription.text = "lost_claimed_description".localized()
                self.viewAlert.isHidden = true
            }, parentController: self)
        case .disputed:
            UIManager.showAlert(title: "womp_womp".localized(), message: "loser_confirm_description".localized(), buttons: ["cancel".localized(), "yes".localized()], completion: { (index) in
                if index == 1 {
                    self.bet?.status = .betLost
                    self.loadStatus()
                }
                self.viewClaim.isHidden = false
                self.lblClaim.text = "lost_claimed".localized()
                self.lblClaimDescription.text = "lost_claimed_description".localized()
                self.viewAlert.isHidden = true
            }, parentController: self)
        default:
            break
        }
    }
    
    @IBAction func onGreen(_ sender: Any) {
        guard let bet = self.bet, let status = bet.status else {
            return
        }
        
        switch status {
        case .live:
            UIManager.showAlert(title: "winner".localized(), message: "winner_confirm_description".localized(), buttons: ["cancel".localized(), "yes".localized()], completion: { (index) in
                if index == 1 {
                    self.bet?.status = .betWon
                    self.loadStatus()
                }
                self.viewClaim.isHidden = false
                self.lblClaim.text = "winning_claimed".localized()
                self.lblClaimDescription.text = "winning_claimed_description".localized()
                self.viewAlert.isHidden = true
            }, parentController: self)
        case .disputed:
            UIManager.showAlert(title: "winner".localized(), message: "winner_confirm_description".localized(), buttons: ["cancel".localized(), "yes".localized()], completion: { (index) in
                if index == 1 {
                    self.bet?.status = .betWon
                    self.loadStatus()
                }
                self.viewClaim.isHidden = false
                self.lblClaim.text = "winning_claimed".localized()
                self.lblClaimDescription.text = "winning_claimed_description".localized()
                self.viewAlert.isHidden = true
            }, parentController: self)

        default:
            break
        }
    }
    
    //View Do or Cancel
    @IBAction func onDo(_ sender: Any) {
        guard let bet = self.bet, let status = bet.status else {
            return
        }
        
        switch status {
        case .disputed:
            break
        default:
            break
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        guard let bet = self.bet, let status = bet.status else {
            return
        }
        
        switch status {
        case .pendingBetActionNeeded:
            UIManager.showAlert(title: "decline_bet".localized(), message: "winner_confirm_description".localized(), buttons: ["cancel".localized(), "decline".localized()], completion: { (index) in
                if index == 1 {
                    self.bet?.status = .expired
                    self.loadStatus()
                }
            }, parentController: self)
        case .disputed:
            break
        default:
            break
        }
    }
}

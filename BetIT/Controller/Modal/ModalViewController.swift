//
//  ModalViewController.swift
//
//  Created by Cesar Villamil on 7/31/19.
//  Copyright © 2019 MajestykApps. All rights reserved.
//


import UIKit

class ModalViewController: UIViewController {
    @IBOutlet weak var viewContainer: UIView!
    var isAnimated: Bool = false

    // Status Bar Style
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isAnimated == false {
            self.viewContainer.frame = CGRect(x: 0, y: self.view.bounds.height, width: viewContainer.bounds.width, height: viewContainer.bounds.height)
            UIView.animate(withDuration: 0.3) {
                self.viewContainer.frame = CGRect(x: 0, y: 54, width: self.viewContainer.bounds.width, height: self.viewContainer.bounds.height)
            }
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {        
        UIView.animate(withDuration: 0.3, animations: {
            self.viewContainer.frame = CGRect(x: self.viewContainer.frame.minX, y: self.viewContainer.frame.minY + self.viewContainer.bounds.height, width: self.viewContainer.bounds.width, height: self.viewContainer.bounds.height)
        }) { _ in
            super.dismiss(animated: true, completion: nil)
        }
    }
}
